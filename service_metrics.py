from __future__ import division

from domonit.containers import Containers
from domonit.ids import Ids
from domonit.inspect import Inspect
from domonit.logs import Logs
from domonit.process import Process
from domonit.changes import Changes
from domonit.stats import Stats
from domonit.exec_command import ExecCommand
from domonit.services import Services
from domonit.config import Config

import json
import ast
import requests

conf=Config()
base_url=conf.get_base_url()

s = Services(base_url)
s_data=s.services()
if 'message' in s_data:
    print(s_data['message'])
else:
    resp={}
    resp['no_of_services']=len(s_data)
    resp['services']=[]
    for d in s_data:
        temp={}
        temp['name']=d['Spec']['Name']
        temp['created_at']=d['CreatedAt']

        if 'Replicated' in d['Spec']['Mode']:
            temp['replicas']=d['Spec']['Mode']['Replicated']['Replicas']
        temp['id']=d['ID']
        if 'com.docker.stack.image' in d['Spec']['Labels']:
            temp['stack_image']=d['Spec']['Labels']['com.docker.stack.image']
        temp['image']=d['Spec']['TaskTemplate']['ContainerSpec']['Image']
        resp['services'].append(temp)

    print(json.dumps(resp))