#!/usr/bin/env python

from __future__ import division

from domonit.containers import Containers
from domonit.ids import Ids
from domonit.inspect import Inspect
from domonit.logs import Logs
from domonit.process import Process
from domonit.changes import Changes
from domonit.stats import Stats
from domonit.exec_command import ExecCommand
from domonit.services import Services

import json
import ast
import requests
import sys
from domonit.config import Config

conf=Config()
base_url=conf.get_base_url()

c = Containers(base_url)
s = Services(base_url)
con = c.containers()

container_count = len(con)

data={}
for co in con:
    temp={}
    temp['id']=co['Id']
    sta = Stats(base_url, co['Id'])

    ec = ExecCommand(base_url, co['Id'])
    if 1<len(sys.argv):
        exid = ec.create_exec(sys.argv[1])
    else:
        exid = ec.create_exec('ls -ltr')
    exeout = ec.start_exec(exid)
    print(exeout)