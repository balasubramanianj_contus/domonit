from domonit.volumes import Volumes
from domonit.config import Config
import json

conf=Config()
base_url = conf.get_base_url()

v=Volumes(base_url)
v_data=v.volumes()
print(json.dumps(v_data))