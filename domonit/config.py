
class Config(object):
    """docstring for Config"""
    # hostname = 'contus-dt-048'
    hostname = '192.168.4.224'
    # hostname = "http+unix://%2Fvar%2Frun%2Fdocker.sock"
    port='2376'
    base=''

    def __init__(self):
        self.set_base_url()

    def set_hostname(self, hostname):
        self.hostname=hostname
        self.set_base_url()

    def set_port(self, port):
        self.port=port
        self.set_base_url()

    def get_base_url(self):
        return self.base

    def set_base_url(self):
        if '.sock' in self.hostname:
            self.base=self.hostname
        else:
            self.base="http://"+self.hostname+":"+self.port

        
# BASE = "http+unix://%2Fvar%2Frun%2Fdocker.sock"
# BASE="http://192.168.5.67:2376"
# BASE="http://contus-dt-048:2376"
