import requests_unixsocket
import json
from errors import BadParameterError, ServerErrorError

from utils.utils import Utils
from config import Config
u = Utils()


#https://docs.docker.com/engine/reference/api/docker_remote_api_v1.24/
class Nodes():

    def __init__(self, base_url):        
        # self.base = "http+unix://%2Fvar%2Frun%2Fdocker.sock"
        # self.base = "http://192.168.5.67:2376"

        self.base=base_url
        self.url = "/nodes"
        
        self.session = requests_unixsocket.Session()
        try:
            self.resp = self.session.get( self.base + self.url)
        except Exception as ex:
            template = "An exception of type {0} occured. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print message

    def nodes(self):
        resp = self.resp
        url = self.url

        resp_status_code = resp.status_code
        u.check_resp(resp_status_code, url)       
        return resp.json()