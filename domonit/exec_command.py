import requests_unixsocket
import json, requests

from utils.utils import Utils
from config import Config
u = Utils()



#https://docs.docker.com/engine/reference/api/docker_remote_api_v1.24/
class ExecCommand():


    def __init__(self, base_url, container_id):
        self.container_id = container_id

        self.base=base_url
        self.session = requests_unixsocket.Session()

    def create_exec(self, command):
        try:
            URL = self.base+"/containers/%s/exec" % (self.container_id)
            PARAMS = '''{"AttachStdin": true,"AttachStdout": true,"AttachStderr": true,"Cmd": ["sh", "-c", "'''+command+'''"],"DetachKeys": "ctrl-p,ctrl-q","Privileged": true,"Tty": true, "User": "root"}'''
            headers = {"Content-type": "application/json"}
            # r = requests.post(url = URL, data=PARAMS, headers=headers)
            r = self.session.post(url = URL, data=PARAMS, headers=headers)
            data = r.json()
            if r.status_code==201:
                exec_id = (data['Id'])
            else:
                exec_id=False
            # print("Exec id : ", exec_id)
            return exec_id
        except Exception as e:
            print(e)
            return 0

    def start_exec(self, exec_id):
        URL = self.base+"/exec/%s/start" % (exec_id)
        PARAMS = '''{"Detach": false,"Tty": false}'''
        headers = {"Content-type": "application/json"}
        # r = requests.post(url = URL, data=PARAMS, headers=headers)
        r = self.session.post(url = URL, data=PARAMS, headers=headers)
        if r.status_code==200:
            data = r.text
        else:
            data=False
        # print("Exec cmd output : ", str(data))
        return data
