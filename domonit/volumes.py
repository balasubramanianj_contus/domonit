import requests_unixsocket
import json
from errors import BadParameterError, ServerErrorError

from utils.utils import Utils
from config import Config
u = Utils()


class Volumes():

    def __init__(self, base_url):
        self.base=base_url
        self.url = "/volumes"
        
        self.session = requests_unixsocket.Session()
        try:
            self.resp = self.session.get( self.base + self.url)
        except Exception as ex:
            template = "An exception of type {0} occured. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print message

    def volumes(self):
        resp = self.resp
        url = self.url

        resp_status_code = resp.status_code
        u.check_resp(resp_status_code, url)       
        return resp.json()