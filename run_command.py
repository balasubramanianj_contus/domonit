from __future__ import division

from domonit.containers import Containers
from domonit.ids import Ids
from domonit.inspect import Inspect
from domonit.logs import Logs
from domonit.process import Process
from domonit.changes import Changes
from domonit.stats import Stats
from domonit.exec_command import ExecCommand
from domonit.services import Services


class RunCommand():
    def __init__(self):
        pass
        
    def collect_containers(self):
        c = Containers()
        con = c.containers()
        conlen=len(con)
        print("No| Container ID,                                                    | Name")
        print("-----------------------------------------------------------------------------")
        for k, co in enumerate(con):
            print(str(k)+" | "+co['Id']+" | "+ co['Names'][0])
        print("-----------------------------------------------------------------------------")
        in_con_no = raw_input("Choose container list number [ex:0]: ")
        if in_con_no and int(in_con_no)<conlen:
            # print(con[int(in_con_no)]['Id'])
            con_id = con[int(in_con_no)]['Id']
            ec = ExecCommand(con_id)
            in_exec_cmd = raw_input("Enter command to run in container [ex:ls -ltr]: ")
            exid = ec.create_exec(in_exec_cmd)
            print("Exec command ID : "+ str(exid))
            exec_data=ec.start_exec(exid)
            print("Exec command output : ")
            print(str(exec_data.encode('utf-8')))

        else:
            print("Invalid container number")

rc = RunCommand()
rc.collect_containers()