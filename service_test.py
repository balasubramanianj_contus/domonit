#!/usr/bin/env python

from __future__ import division

from domonit.containers import Containers
from domonit.ids import Ids
from domonit.inspect import Inspect
from domonit.logs import Logs
from domonit.process import Process
from domonit.changes import Changes
from domonit.stats import Stats
from domonit.exec_command import ExecCommand
from domonit.services import Services
from domonit.config import Config

import json
import ast
import requests

conf=Config()
base_url=conf.get_base_url()

s = Services(base_url)
print( json.dumps(s.services()))