from __future__ import division

from domonit.containers import Containers
from domonit.ids import Ids
from domonit.inspect import Inspect
from domonit.logs import Logs
from domonit.process import Process
from domonit.changes import Changes
from domonit.stats import Stats
from domonit.exec_command import ExecCommand
from domonit.services import Services
from domonit.nodes import Nodes
from domonit.config import Config

import json
import ast
import requests


def containerdata(condata, base_url):
    container_count = len(condata)
    data={}
    data['no_of_containers']=container_count
    data['containers']=[]
    for co in condata:
        temp={}
        temp['id']=co['Id']
        temp['image']=co['Image']
        temp['status']=co['Status']
        temp['name']=co['Names'][0]

        if "Labels" in co:
            if 'com.docker.swarm.service.name' in co['Labels']:
                temp['service_name']=co['Labels']['com.docker.swarm.service.name']
                temp['service_id']=co['Labels']['com.docker.swarm.service.id']
                temp['node_id']=co['Labels']['com.docker.swarm.node.id']

        sta = Stats(base_url, co['Id'])

        cpu_total_usage=(sta.cpu_stats_total_usage())

        # Memory usage
        mem_u = sta.memory_stats_usage()
        # Memory limit
        mem_l = sta.memory_stats_limit()
        # Memory usage %
        # print ("\n#Memory usage %")
        mem_u_p = int(mem_u)*100/int(mem_l)
        temp['mem_usage']=mem_u
        temp['mem_limit']=mem_l
        temp['mem_usage_per']=mem_u_p
        temp['cpu_total_usage']=cpu_total_usage

        inter=sta.interfaces()
        inter=ast.literal_eval(inter)
        temp['received_bytes']=sta.rx_bytes(inter[0])
        temp['transmitted_bytes']=sta.rx_bytes(inter[0])

        data['containers'].append(temp)
    
    # print(json.dumps(data))
    return data

def servicedata(servdata):
    resp={}
    resp['no_of_services']=len(servdata)
    resp['services']=[]
    for d in servdata:
        temp={}
        temp['name']=d['Spec']['Name']
        temp['created_at']=d['CreatedAt']

        if 'Replicated' in d['Spec']['Mode']:
            temp['replicas']=d['Spec']['Mode']['Replicated']['Replicas']
        temp['id']=d['ID']
        if 'com.docker.stack.image' in d['Spec']['Labels']:
            temp['stack_image']=d['Spec']['Labels']['com.docker.stack.image']
        temp['image']=d['Spec']['TaskTemplate']['ContainerSpec']['Image']
        resp['services'].append(temp)

    # print(json.dumps(resp))
    return resp

def nodedata(n_data):
    no_of_nodes = len(n_data)
    response['no_of_nodes']=no_of_nodes
    response['nodes']=[]

    for nod in n_data:
        tmp={}
        tmp['node_id']=str(nod['ID'])
        # tmp['hostname']=str(nod['Description']['Hostname'])
        tmp['hostname']=str(nod['Status']['Addr'])
        tmp['role']=str(nod['Spec']['Role'])
        tmp['availability']=str(nod['Spec']['Availability'])
        if 'ManagerStatus' in nod:
            tmp['reachability']=str(nod['ManagerStatus']['Reachability'])
            tmp['leader']=str(nod['ManagerStatus'].get('Leader', None))
        
        conf.set_hostname(tmp['hostname'])
        con=Containers(conf.get_base_url())
        print('collecting container data...')
        print("==================================================")
        tmp['container_data']=(containerdata(con.containers(), conf.get_base_url()))

        if tmp['role']=='manager':
            serv = Services(conf.get_base_url())
            print('collecting service data...')
            print("==================================================")
            tmp['service_data']=(servicedata(serv.services()))
        response['nodes'].append(tmp)

    print(json.dumps(response))

response={}
conf=Config()
base_url = conf.get_base_url()

n = Nodes(base_url)
n_data = n.nodes()
if 'message' in n_data:
    print(n_data['message'])
else:
    nodedata(n_data)



