# Env Config
config.json
```
{
    "node":3,
    "manager":3,
    "worker":0,
    "service":{
        "helloworld":{
            "replicas":3
        }
    },
    "hostname":"192.168.4.225",
    "port":"2376"
}
```
### - node
```
count of node to be connected.
```

### - manager
```
count of required manager node.
```
### - worker
```
count of required worker node.
```
### - service
```
Give the service name ["helloworld"] and the replicas ["3"].
```
### - hostname
```
Docker leader node IP
```
### - port
```
Docker daemon mode PORT
```

# Validation alert message 
###### #TODO need more validation

### - node mismatch 
Please check connected node's. Some node's are missing!
### - node matched!
All node's are connected.
### - node availability success
All node's are active.
### - node availability drain
Some node's are drained, please make it active. DRAIN node(s) {count}

### - node status check success
All node's are ready!
### - node status check fail
Some node's are down, please make it ready. DOWN node(s) {count}

### - manager matched!
All manager node's are running successfully.
### - manager mismatch
Some manager node's are missing!.

### - worker matched!
All worker node's are running successfully.
### - worker mismatch
Some worker node's are missing!.


### - Service replicas mismatch
{service_name} : need {count} more replicas.
### - Service replicas matched
{service_name} : Replicas mached!
### - Service down
{service_name} : Service down!

# Run
```
python monitor.py
```
