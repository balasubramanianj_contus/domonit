from domonit.networks import Networks
from domonit.config import Config
import json

conf=Config()
base_url = conf.get_base_url()

n=Networks(base_url)
n_data=n.networks()
print(json.dumps(n_data))