from settings import CONFIG
import requests_unixsocket
from domonit.errors import NoSuchContainerError, ServerErrorError, BadParameterError
from domonit.stats import Stats
import ast
import logging
import json

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    CYAN = '\033[96m'
    MAGENTA = '\033[95m'
    GREY = '\033[90m'

class Monitor(object):

    base_url=None
    hostname=None
    session=None

    def __init__(self):
        self.set_base_url(CONFIG['hostname'])
        self.session = requests_unixsocket.Session()


    def set_base_url(self, hostname):
        if '.sock' in hostname:
            self.base_url=hostname
        else:
            self.base_url="http://"+hostname+":"+CONFIG['port']

    def get_base_url(self):
        return self.base_url

    def check_resp(self, resp_status_code, url):
        if resp_status_code == 404:
            raise NoSuchContainerError('GET ' + url + ' {} '.format(resp_status_code)) 
        elif resp_status_code == 500:
            raise ServerErrorError('GET ' + url + ' {} '.format(resp_status_code))
        elif resp_status_code == 400:
            raise BadParameterError('GET ' + url + ' {} '.format(resp_status_code))


    def api_request(self, url):
    	try:
    	    resp = self.session.get( self.base_url+url)

    	    resp_status_code = resp.status_code
    	    self.check_resp(resp_status_code, url)       
    	    return resp.json()
    	except Exception as ex:
    	    template = "An exception of type {0} occured. Arguments:\n{1!r}"
    	    message = template.format(type(ex).__name__, ex.args)
    	    return message

    # scan node datas
    def scan_nodes(self):
        url = "/nodes"
        data=self.api_request(url)
        return data

    def check_node_count(self,node_count):
    	if (CONFIG['node']<node_count):
    		print(bcolors.FAIL+"Please check connected node's. Extra "+ str(node_count-CONFIG['node'])+" node connected."+  bcolors.ENDC)
    	elif (CONFIG['node']>node_count):
    		print(bcolors.FAIL+"Please check connected node's. Need "+ str(CONFIG['node']-node_count)+" more node to be connect." +bcolors.ENDC)
    	else:
    		print(bcolors.OKGREEN+"All node's are connected."+bcolors.ENDC)

    def check_roles(self,roles):
    	if CONFIG['manager'] < roles['manager']:
    		print(bcolors.FAIL+"Manager node exceeds the limit, which is defined in the config."+bcolors.ENDC)
    	elif CONFIG['manager'] > roles['manager']:
    		print(bcolors.FAIL+"Need "+str(CONFIG['manager']-roles['manager'])+" more manager node, it's not meet the requirement, which is defined in the config."+bcolors.ENDC)
    	else:
    		print(bcolors.OKGREEN+"All manager node's are running successfully."+bcolors.ENDC)

    	if roles.get('worker', None) and CONFIG['worker'] < roles['worker']:
    		print(bcolors.FAIL+"Worker's node exceeds the limit, which is defined in the config."+bcolors.ENDC)
    	elif roles.get('worker', None) and CONFIG['worker'] > roles['worker']:
    		print(bcolors.FAIL+"Need "+str(CONFIG['worker']-roles['manager'])+" more worker node, it's not meet the requirement, which is defined in the config."+bcolors.ENDC)
    	elif roles.get('worker', None) and roles['worker']==CONFIG['worker']:
    		print(bcolors.OKGREEN+"All worker node's are running successfully."+bcolors.ENDC)
    	elif CONFIG['worker']:
    		print(bcolors.FAIL+"Some worker node's are missing!."+bcolors.ENDC)

    def check_availability(self,availdata, node_count):
    	if availdata['active']==node_count:
    		print(bcolors.OKGREEN+"All node's are active!"+bcolors.ENDC)
    	if 'drain' in availdata:
    		print(bcolors.FAIL+"Some node's are drained, please make it active. DRAIN node(s) : "+ str(availdata['drain'])+bcolors.ENDC)
    	if 'down' in availdata:
    		print(bcolors.FAIL+"Some node's are down, please make it UP. DOWN node(s) : "+ str(availdata['drain'])+bcolors.ENDC)

    def check_node_status(self,statusdata, node_count):
    	if statusdata['ready']==node_count:
    		print(bcolors.OKGREEN+"All node's are ready!"+bcolors.ENDC)
    	if 'down' in statusdata:
    		print(bcolors.FAIL+"Some node's are down, please make it ready. DOWN node(s) : "+ str(statusdata['down'])+bcolors.ENDC)

    def parse_node_data(self, data):
    	response={}
    	response['node_count']=len(data)
    	response['node_data']=[]

    	leader_count=0
    	avail={}
    	reach={}
    	rolec={}
    	status={}

    	for k,node in enumerate(data):
    		tmp={}
    		tmp['node_id']=str(node['ID'])
    		# tmp['hostname']=str(node['Description']['Hostname'])
    		tmp['hostname']=str(node['Status']['Addr'])
    		tmp['status']=str(node['Status']['State'])
    		if tmp['status'] in status:
    			status[tmp['status']]+=1
    		else:
    			status[tmp['status']]=1
    		
    		tmp['role']=str(node['Spec']['Role'])
    		if tmp['role'] in rolec:
    			rolec[tmp['role']]+=1
    		else:
    			rolec[tmp['role']]=1

    		tmp['availability']=str(node['Spec']['Availability'])
    		if tmp['availability'] in avail:
    			avail[tmp['availability']]+=1
    		else:
    			avail[tmp['availability']]=1

    		if 'ManagerStatus' in node:
    		    tmp['reachability']=str(node['ManagerStatus']['Reachability'])
    		    if tmp['reachability'] in reach:
    		    	reach[tmp['reachability']]+=1
    		    else:
    		    	reach[tmp['reachability']]=1
    		    tmp['leader']=(node['ManagerStatus'].get('Leader', None))
    		    leader_count+= 1 if tmp['leader'] else 0

    		response['node_data'].append(tmp)
    		print(bcolors.WARNING+"-----Node "+str(k)+"-----------------------------------")
    		print "Node " +str(k)+" : "+str(tmp['hostname'])+" | "+str(tmp['role'])+" | "+str(tmp['availability']) +" | "+str(tmp['status']) +str(" | Leader " if tmp.get('leader', None) else "")
    		print("----------------------------------------------"+bcolors.ENDC)

    		self.set_base_url(tmp['hostname'])

    		if tmp['status']=='ready' and tmp['role']=='manager':
		    	vol_data=self.scan_volumes()
		    	volnames=self.get_all_volume_names(vol_data)
    		else:
		    	volnames=[]

	    	if tmp.get('leader', 0) and CONFIG['service_check']:
		    	netw_data=self.scan_networks()
		    	nids=self.get_all_network_ids(netw_data)
		    	
    			serv_data=self.scan_services()
    			self.parse_service_data(serv_data, nids, volnames)

    		if tmp['status']=='ready' and CONFIG['container_check']:
	    		con_data=self.scan_containers()
	    		self.parse_container_data(con_data, volnames)
    		
    	print(bcolors.OKBLUE+"----------------------------------------------"+bcolors.ENDC)
    	
    	print(bcolors.OKBLUE+"-----Node stats--------------------------------")
    	print("Total connecting nodes : "+str(response['node_count']))
    	print("Leader : "+str(leader_count))
    	print("Role : "+str(rolec))
    	print("Availability : "+str(avail))
    	print("Status : "+str(status))
    	print("Reachability : "+str(reach))
    	print("----------------------------------------------"+bcolors.ENDC)
    	self.check_node_count(response['node_count'])
    	self.check_roles(rolec)
    	self.check_availability(avail, response['node_count'])
    	self.check_node_status(status, response['node_count'])

    def scan_containers(self):
    	url = "/containers/json"
        data=self.api_request(url)
        return data

    def parse_container_data(self, condata, volumes):
    	container_count = len(condata)
    	vol_container_count = 0
    	data={}
    	data['no_of_containers']=container_count
    	print(bcolors.CYAN+"-----Total Containers : "+str(container_count)+"----------------------"+bcolors.ENDC)
    	data['containers']=[]
    	for k,co in enumerate(condata):
    	    # print(bcolors.CYAN+"-----Container "+str(k)+"------------------------------"+bcolors.ENDC)
    	    temp={}
    	    temp['id']=co['Id']
    	    temp['image']=co['Image']
    	    temp['status']=co['Status']
    	    temp['name']=co['Names'][0]
    	    temp['volume']=[]

    	    # print("Id : "+str(temp['id']))

    	    if "Labels" in co:
    	        if 'com.docker.swarm.service.name' in co['Labels']:
    	            temp['service_name']=co['Labels']['com.docker.swarm.service.name']
    	            temp['service_id']=co['Labels']['com.docker.swarm.service.id']
    	            temp['node_id']=co['Labels']['com.docker.swarm.node.id']

            if 'Mounts' in co:
            	for mount in co['Mounts']:
            		if mount['Type']=='volume':
            			vol_container_count+=1
	            		volu=str(mount['Name'])
	            		if volu not in volumes:
	            			print(bcolors.FAIL+"Volume '"+volu+"' is not available."+bcolors.ENDC)
	            		else:
	            			print(bcolors.OKGREEN+"Volume '"+volu+"' is available"+bcolors.ENDC)
	            		temp['volume'].append(volu)

    	    sta = Stats(self.base_url, co['Id'])

    	    cpu_total_usage=(sta.cpu_stats_total_usage())

    	    # Memory usage
    	    mem_u = sta.memory_stats_usage()
    	    # Memory limit
    	    mem_l = sta.memory_stats_limit()
    	    # Memory usage %
    	    # print ("\n#Memory usage %")
    	    mem_u_p = int(mem_u)*100/int(mem_l) if mem_u else 0
    	    temp['mem_usage']=mem_u
    	    temp['mem_limit']=mem_l
    	    temp['mem_usage_per']=mem_u_p
    	    temp['cpu_total_usage']=cpu_total_usage

    	    inter=sta.interfaces()
    	    if inter:
	    	    inter=ast.literal_eval(inter)
	    	    temp['received_bytes']=sta.rx_bytes(inter[0])
	    	    temp['transmitted_bytes']=sta.rx_bytes(inter[0])

    	    data['containers'].append(temp)
    	    # print("Name : "+str(temp['name']))
    	    # print("Status : "+str(temp['status']))
    	    # print("Mem. Usage : "+str(temp['mem_usage'])+" bytes")
    	    # print("Mem. limit : "+str(temp['mem_limit'])+" bytes")
    	    # print("CPU Usage : "+str(temp['cpu_total_usage']))
    	    # print("Image : "+str(temp['image']))
    	    # print(bcolors.CYAN+"----------------------------------------------"+bcolors.ENDC)
    	# print(json.dumps(data))
    	print(bcolors.CYAN+str(vol_container_count)+" volumes attached to containers."+bcolors.ENDC)
    	return data

    def scan_services(self):
        url = "/services"
        data=self.api_request(url)
        return data

    def parse_service_data(self, servdata, nids=[], volumes=[]):
        resp={}
        down_network={}
        up_network={}
        con_services=None
        resp['no_of_services']=len(servdata)
        print(bcolors.CYAN+"-----Total Services : "+str(resp['no_of_services'])+"----------------------"+bcolors.ENDC)
        resp['services']=[]
        for k, d in enumerate(servdata):
            temp={}
            temp['name']=d['Spec']['Name']
            temp['created_at']=d['CreatedAt']

            if 'Replicated' in d['Spec']['Mode']:
                temp['replicas']=d['Spec']['Mode']['Replicated']['Replicas']
            temp['id']=d['ID']
            if 'com.docker.stack.image' in d['Spec']['Labels']:
                temp['stack_image']=d['Spec']['Labels']['com.docker.stack.image']
            temp['image']=d['Spec']['TaskTemplate']['ContainerSpec']['Image']
            temp['network']=[]
            temp['volume']=[]

            if 'Networks' in d['Spec']['TaskTemplate']:
	            for ne in d['Spec']['TaskTemplate']['Networks']:
	            	nid=str(ne['Target'])
	            	if nid not in nids:
	            		down_network[nid]=1
	            		# print(bcolors.FAIL+"Network '"+nid+"' is not available."+bcolors.ENDC)
	            	else:
	            		up_network[nid]=1
	            		# print(bcolors.OKGREEN+"Network '"+nid+"' is available"+bcolors.ENDC)
	            	temp['network'].append(nid)
            # if 'Mounts' in d['Spec']['TaskTemplate']['ContainerSpec']:
            # 	for mount in d['Spec']['TaskTemplate']['ContainerSpec']['Mounts']:
            # 		if mount['Type']=='volume':
	           #  		volu=str(mount['Source'])
	           #  		if volu not in volumes:
	           #  			print(bcolors.FAIL+volu+": Volume is not available."+bcolors.ENDC)
	           #  		else:
	           #  			print(bcolors.OKGREEN+volu+": Volume is available"+bcolors.ENDC)
	           #  		temp['volume'].append(volu)

            resp['services'].append(temp)


            # print(bcolors.CYAN+"-----Service "+str(k)+"------------------------------"+bcolors.ENDC)
            # print("Name : "+str(temp['name']))
            # print("Replicas : "+str(temp.get('replicas', None)))
            # print("Image : "+str(temp['image']))
            # print("Networks : "+str(temp['network']))
            # print(bcolors.CYAN+"----------------------------------------------"+bcolors.ENDC)

            con_services = CONFIG['service']        			
            if temp['name'] in CONFIG['service']:
            	currepl = temp.get('replicas', None)
            	conrepl = CONFIG['service'][temp['name']]['replicas']
            	if currepl and conrepl < currepl:
            		print(bcolors.FAIL+temp['name'] + " : extra "+ str(currepl - conrepl)+" more replicas running."+bcolors.ENDC)
            	if currepl and conrepl > currepl:
            		print(bcolors.FAIL+temp['name'] + " : need "+ str(conrepl - currepl)+" more replicas."+bcolors.ENDC)
            	elif currepl and conrepl == currepl:
            		print(bcolors.OKGREEN+temp['name'] + " : Replicas mached!"+bcolors.ENDC)
            	elif currepl:
            		print(bcolors.FAIL+temp['name'] + " : Replicas doesn't mached!"+bcolors.ENDC)
            	con_services.pop(temp['name'], None)

        for ms in con_services:
        	print(bcolors.FAIL+ms + " : Service down!"+bcolors.ENDC)



        # print(json.dumps(resp))
        if len(down_network) > 0:
            print(bcolors.FAIL+str(len(down_network))+": Networks is not available"+bcolors.ENDC)
            print(bcolors.FAIL+str(down_network)+bcolors.ENDC)
        else:
            print(bcolors.OKGREEN+str(len(up_network))+": All Networks are available"+bcolors.ENDC)
        return resp

    def scan_networks(self):
        url = "/networks"
        data=self.api_request(url)
        return data

    def get_all_network_ids(self, netdata):
    	ids=[str(net['Id']) for net in netdata]
    	return ids

    def scan_volumes(self):
        url = "/volumes"
        data=self.api_request(url)
        return data

    def get_all_volume_names(self, voldata):
    	# print(voldata)
    	volnames=[str(vol['Name']) for vol in voldata['Volumes']]
    	return volnames



try:
	mon = Monitor()
	n_data=mon.scan_nodes()
	if type(n_data) is list:
		mon.parse_node_data(n_data)
	else:
		print(n_data)
except Exception as e:
	raise
