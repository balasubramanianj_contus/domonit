#!/usr/bin/env python

from __future__ import division

from domonit.containers import Containers
from domonit.ids import Ids
from domonit.inspect import Inspect
from domonit.logs import Logs
from domonit.process import Process
from domonit.changes import Changes
from domonit.stats import Stats
from domonit.nodes import Nodes
from domonit.config import Config

import json
import ast

conf=Config()
base_url=conf.get_base_url()

c = Containers(base_url)
con = c.containers()
container_count = len(con)

nod = Nodes(base_url)
nodes=nod.nodes()
print(nodes)
node_count= len(nodes)


data={}
data['no_of_containers']=container_count
data['containers']=[]
data['no_of_nodes']=node_count
data['nodes']=[]

for node in nodes:
    temp={}
    temp['id']=node['ID']
    if 'deploy.role' in node['Spec']['Labels']:
        temp['role']=node['Spec']['Labels']['deploy.role']
    else:
        temp['role']=node['Spec']['Role']
    data['nodes'].append(temp)


for co in con:
    temp={}
    temp['id']=co['Id']
    temp['image']=co['Image']
    temp['status']=co['Status']
    temp['name']=co['Names'][0]

    if "Labels" in co:
        if 'com.docker.swarm.service.name' in co['Labels']:
            temp['service_name']=co['Labels']['com.docker.swarm.service.name']
            temp['service_id']=co['Labels']['com.docker.swarm.service.id']
            temp['node_id']=co['Labels']['com.docker.swarm.node.id']


    sta = Stats(base_url, co['Id'])

    cpu_total_usage=(sta.cpu_stats_total_usage())
    # rb=sta.rx_bytes(str(inter))

    # Memory usage
    mem_u = sta.memory_stats_usage()
    # Memory limit
    mem_l = sta.memory_stats_limit()
    # Memory usage %
    # print ("\n#Memory usage %")
    mem_u_p = int(mem_u)*100/int(mem_l)
    temp['mem_usage']=mem_u
    temp['mem_limit']=mem_l
    temp['mem_usage_per']=mem_u_p
    temp['cpu_total_usage']=cpu_total_usage

    inter=sta.interfaces()
    inter=ast.literal_eval(inter)
    temp['received_bytes']=sta.rx_bytes(inter[0])
    temp['transmitted_bytes']=sta.rx_bytes(inter[0])

    data['containers'].append(temp)
print(json.dumps(data))
